![](Aspose.Words.dab1026d-efa1-4ea0-ad27-aacebc56f85e.001.png)

***Text Mining AND Sentiment analysis Project***

**PREPARED BY AZIZAGHA ABASZADA** 

Summary information 

In this project, a topic was chosen and the texts written on Twitter about that topic were  taken.  This  text  was  sampled  from  the  data  and  divided  into  certain categories. Subsequently, these sample data were taught to artificial intelligence algorithms and all data were divided into certain categories with these algorithms. Finally, sentiment analysis was performed on the categorized data, and it was revealed that people said negative or positive information for each category. 

**Subject Heading: Online Education Data Source: Twitter** 

**Data Size: 6391 tweet (unedited data)** 

**About Subject:** 

The preparation of this project was made during the Covid-19 pandemic period. And one of the topics on the agenda of the term was online education. And in this project there is information about **Online education** that people share on Twitter. **Note**: These data are taken from tweets in Turkish. 

**Note**: Since this project has to be done in Turkish, Turkish data were used. **Python Code:**  

https://gitlab.com/Aziz9900/Sentiment-Analysis/- /blob/main/Predictive%20Analysis%20and%20Sentiment%20Analysis.ipynb 

1. **step:**  

Data set is about the word "Uzaktan Eğitim" (it is mean Online Education) was pulled from the Twitter developer account. After these data were added to excel named "Kirli\_veri" (it is mean unedited data).  

2. **step:**  

Then by doing operations on the unedited data using python. Clean data has been extracted. In the data cleaning process, unnecessary words and repetitive data in the data set were removed. 

3. **step:**  

After the cleaning process, the data was saved in an excel named "temiz\_veri" (it is mean clean data). A sample was taken from this clean data data and 4 different categorical data sets were created with 200 data in each category. This dataset was used to train the machine learning algorithms. 

4. **step:**  

**Model Training** 

1. **Import Python libraries and Data set.** 
1. **Import category data set.** 
1. 100 data were drawn for each category from the data set and a category data set  was  created.  These  4  categories  are  live  class,  education,  exam, technology. (in Turkish canli ders, eğitim, sınav, teknoloji). 
1. **Dividing texts into roots and Categorizing dataset as numbers** 
1. **Splitting the dataset into testing and learning** 
1. **Text data is translated into numeric. and thus each text is assigned a numerical value.** 
1. **Preparation of necessary libraries for processing prediction models** 
1. **Testing data in different algorithms** 

**(Data sets were tested with 6 different machine learning algorithms.)** 

9. **Success score on 6 different machine learning algorithm datasets** 

5. **Step:** 

**Predict new data with the Selected Models** 

The  two  most  successful  algorithms  (with  the  highest  prediction  rate)  were chosen to make predictions on the real data set. These are **Random Forest** and **Decision Tree** algorithms. These algorithms will be tested on real data. 

1. **Text data is translated into numeric. and thus each text is assigned a numerical value.** 
1. **Making predictions using the Decision Tree algorithm** 
1. **Making predictions using the Random Forest algorithm** 
1. **Results of predictions** 

**6. Step: Sentiment Analysis (positive or negative estimation of data by categories)** 

After categorizing the data using algorithms, sentiment analysis was performed according to these categories. Turkish bert library was used for sentiment analysis. 
